/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2021 Andreas Heckt
 *
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Broadband railway rooftop antenna modeled on SENCITY® Rail MIMO by HUBER+SUHNER
 * Eisenbahn-Breitband-Dachantenne nach Vorbild SENCITY® Rail MIMO by HUBER+SUHNER
 *
 * Ressources/Quellen
  * [1]: Main measures from data sheet http://www.inexim.pl/Download/Karty%20katalogowe%20RF/Anteny/Sencity%20Rail/1399.99.0130_dataSheet.pdf
 ** [2]: Measures taken from pictures
 */

 // allgemeine Komponenten
include <D:\3D Objects\_OpenSCAD common\rcube.scad>

faktor = 1 / 87; // Maßstab


// Maße in mm

// allgemeine Maße

antenne_l = 352.5; // Länge
antenne_b = 102.5; // Breite
antenne_h = 80.91; // Höhe; 81.6 angepasst an Layer-Höhe 0,03 mm

unterlage_h = 5.22; // Höhe (geschätzt); angepasst an Layer-Höhe 0,03 mm


// Montage
$fn = $preview == true ? 64 : 128;

scale(faktor) {
	antenne();
}


// Antenne
module antenne() {
	end_r = 65; // Radius der Zylindersegmente am Ende (geschätzt)
	end_offset_y = 110; //Abstand der Zylindersegmente vom Ende (geschätzt)

	center_r = 700; // Radius der ausgeschnittenen Zylindersegmente in der Mitte (geschätzt)

	translate([0, 0, unterlage_h]) difference() {
		union() {
			difference() {
				union() {
					for (y = [-1 : 2 : 1]) {
						translate([0, y * end_offset_y, antenne_h / 2]) intersection() {
							translate([antenne_b / 2 - (antenne_b - end_r), 0, 0]) ellipsoid_section(end_r);
							translate([-antenne_b / 2 + (antenne_b - end_r), 0, 0]) ellipsoid_section(end_r);
						}
					}
					translate([0, 0, antenne_h / 2]) cube(size = [antenne_b, antenne_l - end_offset_y, antenne_h], center = true);
				}

				$fn = $preview == true ? 64 : 512;
				translate([-732, 0, antenne_h]) scale([1, 1, 0.7]) sphere(r = center_r);
				translate([732, 0, antenne_h]) scale([1, 1, 0.7]) sphere(r = center_r);
			}
			fuss();
			unterlage();
			zapfen();
		}
		befestigung();
	}
}

// Fuß
module fuss() {
	fuss_h = 18.27; // Höhe (geschätzt); angepasst an Layer-Höhe 0,03 mm
    fuss_r = 28; // Radius (geschätzt)

	translate([0, 0, fuss_h / 2]) rcube([antenne_b, antenne_l, fuss_h], [fuss_r, fuss_r, fuss_r, fuss_r], center = true);
}

// Unterlage
module unterlage() {
	unterlage_l = antenne_l + 10; // Länge (geschätzt)
	unterlage_b = antenne_b + 10; // Breite (geschätzt)
	unterlage_r = 18; // Radius (geschätzt)

	translate([0, 0, -unterlage_h / 2]) 
	rcube([unterlage_b, unterlage_l, unterlage_h], [unterlage_r, unterlage_r, unterlage_r, unterlage_r], center = true);
}

// Befestigungszapfen
module zapfen() {
	zapfen_r = 0.4 * 87; // Radius
	zapfen_h = 1.5 * 87 + unterlage_h; // Höhe
	zapfen_c = 0.15; // Fase für einfachere Montage

	translate([0, 0, -(zapfen_h - zapfen_c * 87) / 2]) cylinder(r = zapfen_r, h = zapfen_h - zapfen_c * 87, center = true);
	translate([0, 0, -zapfen_h + zapfen_c * 87 / 2]) cylinder(r1 = zapfen_r - zapfen_c * 87, r2 = zapfen_r, zapfen_c * 87, center = true);
}

// Befestigung (Aussparung)
module befestigung() {
	befestigung_b = 20; // Breite (geschätzt)
	befestigung_offset_x =  80 + befestigung_b / 2; // Abstand vom Ende (geschätzt)
	befestigung_offset_z = 5.22; // Dicke Bodenplatte (geschätzt)
	befestigung_h = antenne_h - befestigung_offset_z; // Höhe (geschätzt)
	befestigung_t = 20; // Breite (geschätzt)

	for (x = [-1 : 2 : 1]) {
		for (y = [-1 : 2 : 1]) {
			translate([y * (antenne_b - befestigung_b) / 2 + 1, x * (antenne_l / 2 - befestigung_offset_x), befestigung_h / 2 + befestigung_offset_z + 1]) {
				rotate([0, 0, y * 90]) {
					rcube([befestigung_b, befestigung_b + 2, befestigung_h + 2], [0, 0, befestigung_b / 2, befestigung_b / 2], center = true);
				}
				
			}
		}
	}
}

module ellipsoid_section(radius) {
	// end_r = 65; // Radius der Zylindersegmente am Ende (geschätzt)

	translate([0, 0, -antenne_h / 2]) intersection() {
		scale([1, 1, 3]) sphere(r = radius);
		translate([0, 0, antenne_h / 2]) cube(size=[radius * 2, radius * 2, antenne_h], center = true);
	}
}